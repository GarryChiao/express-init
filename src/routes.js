// User Part
const UserController = require('./controllers/UserController')

module.exports = (app) => {
  app.get('/index',
    UserController.index
  )
}
