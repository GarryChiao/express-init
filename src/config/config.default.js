module.exports = {
  // firebase serviceAccount
  testServiceAccount: './config/TSF-test-8b13bd0ce322.json',
  onlineServiceAccount: './config/tsf-online-firebase-adminsdk-ukt0j-da00cb744f.json',
  // express server port
  port: process.env.PORT || 8081,
  defaultReturnRate: 5 / 10000
}
