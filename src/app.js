require('dotenv').config()
// const fs = require('fs')
const http = require('http')
// const https = require('https')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')// shows restful request details
const config = require('./config/config')
// const cron = require('node-cron')

// const admin = require('firebase-admin')
// var testServiceAccount = require(config.testServiceAccount)
// var onlineServiceAccount = require(config.onlineServiceAccount)

// // initialize for test
// admin.initializeApp({
//   credential: admin.credential.cert(testServiceAccount),
//   databaseURL: 'https://tsf-test-5360d.firebaseio.com',
//   storageBucket: 'gs://tsf-test-5360d.appspot.com'
// })

// initialize for online
// admin.initializeApp({
//   credential: admin.credential.cert(onlineServiceAccount),
//   databaseURL: 'https://tsf-online.firebaseio.com',
//   storageBucket: 'gs://tsf-online.appspot.com'
// })

// // after initialize the firebase, call the system controller
// const System = require('./controllers/SystemController')

const app = express()

app.use(morgan('combined'))
app.use(bodyParser.json({limit: '50mb'}))
app.use(cors())

require('./routes')(app)

app.listen(config.port, function () {
  console.log('app running on port ', config.port)
})

app.get('/api-test', (req, res) => {
  res.send('TimeSpaceFinance server api is running!')
})

// // SSL Certificate setting
// if (process.env.NODE_ENV === 'production') {
//   const privateKey = fs.readFileSync('/etc/letsencrypt/live/timespacefintech.com/privkey.pem', 'utf8')
//   const certificate = fs.readFileSync('/etc/letsencrypt/live/timespacefintech.com/cert.pem', 'utf8')
//   const ca = fs.readFileSync('/etc/letsencrypt/live/timespacefintech.com/chain.pem', 'utf8')
//   const credentials = { key: privateKey, cert: certificate, ca: ca }

//   // Starting both http & https servers
//   const httpsServer = https.createServer(credentials, app)

//   const httpServer = http.createServer(app)
//   httpServer.listen(8080, () => {
//     console.log('HTTP Server running on port 8080')
//   })

//   httpsServer.listen(8443, () => {
//     console.log('HTTPS Server running on port 8443')
//   })
// }

const httpServer = http.createServer(app)
httpServer.listen(8080, () => {
  console.log('HTTP Server running on port 8080')
})

// // runs everyday 12:00am
// cron.schedule('0 0 0 * * *', async function () {
//   console.log('calculating daily interest return')
//   System.dailyInterestReturn()
// })

// runs on first day of months
// cron.schedule('* * * 1 * *', async function () {
//   console.log('calculating monthly report')
//   System.calculateMonthlyUserReport()
// })

// cron.schedule('* * * * *', async function () {
//   System.adminFunction()
// })
